'use strict';

const express = require('express');
const indexView = require('../../controllers');
const router = express.Router();

router.get('/', indexView);

module.exports = router;