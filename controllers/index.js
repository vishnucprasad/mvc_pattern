const httpStatusCodes = require("../config/httpStatusCodes");

const indexView = (req, res, next) => {
    res.status(httpStatusCodes.OK).render('index');
};

module.exports = indexView;